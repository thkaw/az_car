﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace AZ_CAR
{
    public class Global : System.Web.HttpApplication
    {

        public CloudBlobContainer container;
        protected void Application_Start(object sender, EventArgs e)
        {
            CloudStorageAccount storageAccount =
CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));



            // initialize blob storage
            CloudBlobClient blobStorage = storageAccount.CreateCloudBlobClient();

            // Create a container name "eventlogs"
            container = blobStorage.GetContainerReference("azcarcmd");


            bool storageInitialized = false;
            while (!storageInitialized)
            {

                // create the blob container and allow public access
                container.CreateIfNotExists();

                container.SetPermissions(
                    new BlobContainerPermissions
                    {
                        PublicAccess =
                            BlobContainerPublicAccessType.Blob
                    });

                storageInitialized = true;

            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}