#######################################
# Autor: Nathaniel Chen               #
# Email: ar801112usase@hotmail.com    #
# Blog : www.ntex.tw                  #
#######################################

import http.client
from threading import Thread
from time import sleep

import RPi.GPIO as GPIO

MOTO_R1 = 15
MOTO_R2 = 16
MOTO_L1 = 18
MOTO_L2 = 22
                      
class AzureConnect(Thread):    
    conn = None

    def __init__(self):
        Thread.__init__(self)
        
    def run(self):
        conn = http.client.HTTPConnection("dxrdaaazcar.azurewebsites.net")
        while True:
            conn.request("GET","/Service1.svc/GetCmd")
            res = conn.getresponse()
            azure_cmd = res.read()
            print(azure_cmd)                                 
            moto_c.moto_drive(int(azure_cmd.decode("utf-8")))
            sleep(0.5)

class MotoControl:           

    def __init__(self):
        # Use physical pin numbers
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(MOTO_R1, GPIO.OUT)
        GPIO.setup(MOTO_R2, GPIO.OUT)
        GPIO.setup(MOTO_L1, GPIO.OUT)
        GPIO.setup(MOTO_L2, GPIO.OUT)
        MotoControl.moto_stop(self)
                  
    def moto_stop(self):
        GPIO.output(MOTO_R1, GPIO.HIGH)
        GPIO.output(MOTO_R2, GPIO.HIGH)
        GPIO.output(MOTO_L1, GPIO.HIGH)
        GPIO.output(MOTO_L2, GPIO.HIGH)
        print(">>>STOP<<<")

    def moto_forward(self):
        GPIO.output(MOTO_R1, GPIO.HIGH)
        GPIO.output(MOTO_R2, GPIO.LOW)
        GPIO.output(MOTO_L1, GPIO.HIGH)
        GPIO.output(MOTO_L2, GPIO.LOW)
        print(">>>MOVE FORWARD<<<")

    def moto_back(self):
        GPIO.output(MOTO_R1, GPIO.LOW)
        GPIO.output(MOTO_R2, GPIO.HIGH)
        GPIO.output(MOTO_L1, GPIO.LOW)
        GPIO.output(MOTO_L2, GPIO.HIGH)
        print(">>>MOVE BACK<<<")

    def moto_left(self):
        GPIO.output(MOTO_R1, GPIO.HIGH)
        GPIO.output(MOTO_R2, GPIO.LOW)
        GPIO.output(MOTO_L1, GPIO.HIGH)
        GPIO.output(MOTO_L2, GPIO.HIGH)
        print(">>>MOVE LEFT<<<")

    def moto_right(self):
        GPIO.output(MOTO_R1, GPIO.HIGH)
        GPIO.output(MOTO_R2, GPIO.HIGH)
        GPIO.output(MOTO_L1, GPIO.HIGH)
        GPIO.output(MOTO_L2, GPIO.LOW)
        print(">>>MOVE RIGHT<<<")

    def moto_drive(self, drive_cmd):
        print("drive_cmd:" + str(drive_cmd))
        if drive_cmd == 0: #STOP
            self.moto_stop()
        elif drive_cmd == 1: #FORWARD
            self.moto_forward()
        elif drive_cmd == 2: #BACK
            self.moto_back()
        elif drive_cmd == 3: #LEFT
            self.moto_left()
        elif drive_cmd == 4: #RIGHT
            self.moto_right()
        else:
            print("Wrong drive_cmd!:%s" % str(drive_cmd))

         
moto_c = MotoControl()    

def main():              
    AzureConnect().start()


if __name__ == '__main__':
        main()  

