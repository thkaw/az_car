﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace UAPP_AZ_CAR
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }


        ///
        /// Forward = 1 Back = 2 left = 3 right = 4 stop = 0
        /// 

        enum CMD
        {
            Stop = 0, Forward = 1,
            Back = 2, Left = 3, Right = 4
        }


        private static async Task SendToAzure(CMD cmd)
        {
            // Using random addition value to avoid webdata cache.                                          
            //Random rr = new Random();
            //int rdn = rr.Next(1, 1000000);


            // TODO: Change to your site name.
            //Uri uri = new Uri("http://dxrdaaazcar.azurewebsites.net/Service1.svc/SetCmd/" + (int)cmd + "?" + rdn);
            Uri uri = new Uri("http://dxrdaaazcar.azurewebsites.net/Service1.svc/SetCmd/" + (int)cmd);

            HttpClient httpClient = new HttpClient();

            // Header add this attribute to avoid cache!, no need add random string anymore.
            httpClient.DefaultRequestHeaders.Add("Cache-Control", "no-cache");

            string result = await httpClient.GetStringAsync(uri);
        }

        private async void btn_stop_Click(object sender, RoutedEventArgs e)
        {
            await SendToAzure(CMD.Stop);
        }

        private async void btn_forward_Click(object sender, RoutedEventArgs e)
        {

            await SendToAzure(CMD.Forward);
        }

        private async void btn_back_Click(object sender, RoutedEventArgs e)
        {
            await SendToAzure(CMD.Back);
        }

        private async void btn_left_Click(object sender, RoutedEventArgs e)
        {
            await SendToAzure(CMD.Left);
        }

        private async void btn_right_Click(object sender, RoutedEventArgs e)
        {
            await SendToAzure(CMD.Right);
        }


    }
}
